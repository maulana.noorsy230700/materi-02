// Arrays - variables that hold multiple values

const numbers = new Array(1, 2, 3, 4, 5);
console.log(numbers);

const fruits = ['grape', 'strawberry', 'banana', 10, true]
fruits[3] = 'blueberry';
fruits.push('avocado');
fruits.unshift('kiwi');
fruits.pop();

console.log(fruits.indexOf('strawberry'));

console.log(Array.isArray('isitarray'));
console.log(fruits[1]);