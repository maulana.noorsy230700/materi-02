// let, const, var

// this is penganut level scope
const age = 43;

age = 22;

// assigning with let

let age1;

age1 = 19;

console.log(age1)