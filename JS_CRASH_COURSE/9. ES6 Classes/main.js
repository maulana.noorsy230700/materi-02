 // Class
 class Person {
    constructor (firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
    } 
    
getBirthYear() {
    return this.dob.getFullYear();
}
getFullName() {
    return `${this.firstName} ${this.lastName}`;
}
}
